# Copyright 2017 - 2018  Ternaris.
# SPDX-License-Identifier: Apache-2.0

"""Command-line interface for ADE"""

import asyncio
import json
import os
import platform
import re
import shutil
import sys
import tempfile
import time
from collections import defaultdict
from pathlib import Path
from subprocess import DEVNULL

import aiohttp
import click
import yarl
from pkg_resources import parse_version

from . import dotenv
from . import __version__ as VERSION
from . import gitlab
from . import registry
from .release import Release
from .config import load_config
from .docker import docker_exec, docker_run, docker_stop, get_label, update_images
from .docker import is_running, print_image_matrix
from .utils import asyncio_run, launch_pdb_on_exception, run, splitjoin


ADERC = None
CFG = None
PDB = os.environ.get('PDB')
STATICX_PROG_PATH = os.environ.get('STATICX_PROG_PATH')
VERSION = parse_version(VERSION)


def _print_version(ctx, _, value):
    if not value or ctx.resilient_parsing:
        return
    from . import __version__
    print(__version__)
    ctx.exit()


@click.group()
@click.option('--version', is_flag=True, callback=_print_version, expose_value=False, is_eager=True)
def ade():
    """ADE Development Environment."""


@ade.command('enter')
@click.option('-u', '--user', help='Enter ade as given user (e.g. root) instead of default')
@click.argument('cmd', required=False)
def ade_enter(cmd, user):
    """Enter environment, running optional command."""
    if not ADERC:
        print('ERROR: Needs to run from within project with .aderc file', file=sys.stderr)
        sys.exit(1)

    if not is_running(CFG.name):
        print('ERROR: ADE is not running, please start with: ade start', file=sys.stderr)
        sys.exit(1)

    docker_exec(CFG.name, cmd, user, broken=CFG.broken_docker_exec)


@ade.command('start')
@click.option('--update/--no-update', help='Pull docker registries for updates. '
              'Using --update will imply --force.')
@click.option('--enter/--no-enter', help='Enter environment after starting.')
@click.option('-f', '--force/--no-force', help='Force restart of running environment.')
@click.option('--select', multiple=True, help=splitjoin("""
    Select image tags to be used instead of configured defaults. Valid
    image tags are git tags and branches. To select one for a specific
    image use ``IMAGE:TAG`` (e.g. ``ade:ftr123``) and only ``TAG`` to select
    it for all images for which it exists (e.g. ``release-42``).
"""))
@click.argument('addargs', nargs=-1)
@asyncio_run
async def ade_start(update, enter, force, addargs, select):
    """Start environment from within ADE project.

    ADDARGS are directly passed to docker run. To pass options,
    separate them with ``--`` from the preceding ade options.

    See https://docs.docker.com/engine/reference/commandline/run/ and
    :ref:`addargs` for more information.
    """
    # pylint: disable=too-many-arguments,too-many-locals,too-many-branches,too-many-statements
    if not ADERC:
        print('ERROR: Needs to run from within project with .aderc file', file=sys.stderr)
        sys.exit(1)

    if not CFG.images:
        print('ERROR: No images defined, please set ADE_IMAGES', file=sys.stderr)
        sys.exit(1)

    if not CFG.home:
        print('ERROR: Not started from within ade home and ADE_HOME not set', file=sys.stderr)
        sys.exit(1)

    if CFG.home == os.environ['HOME']:
        print('ERROR: ADE home must be different from normal $HOME', file=sys.stderr)
        sys.exit(1)

    image_names = defaultdict(list)
    for img in CFG.images:
        image_names[img.name].append(img)
    for name, imgs in image_names.items():
        if len(imgs) > 1:
            msg = ''.join(['\n  {}'.format(img.fqn) for img in imgs])
            print('WARNING: Multiple images named {}:{}'.format(name, msg))
            sys.exit(1)

    selectors = []
    for sel in select:
        try:
            img_name, tag = sel.split(':')
        except ValueError:
            selectors.append((None, sel))
        else:
            selectors.insert(0, (img_name, tag))

    remote_images = []
    for img in CFG.images:
        if img.registry or img.namespace:
            remote_images.append(img)

    if selectors:
        try:
            await registry.adjust_tags(remote_images, selectors)
        except registry.NoSuchImage as e:
            fqn = e.args[0].fqn  # pylint: disable=no-member
            print('ERROR: Image {} does not exist'.format(fqn), file=sys.stderr)
            sys.exit(1)
        except registry.NoSuchTag as e:
            print('ERROR: {} does not exists for {}; choose from:\n  {}'
                  .format(e.args[1], e.args[0], '  \n'.join(sorted(e.args[2]))), file=sys.stderr)
            sys.exit(1)

    if update:
        # Update always implies force.
        force = True
        await update_images(CFG.name, remote_images)

    tags = {x.tag for x in remote_images}
    unused_selectors = [x for x in selectors if x[1] not in tags]
    if unused_selectors:
        print('WARNING: Some select options did not match any images', file=sys.stderr)

    print('Starting {} with the following images:'.format(CFG.name))
    print_image_matrix(None, CFG.images)

    if is_running(CFG.name):
        if not force:
            print('\nERROR: {} is already running.'
                  ' Use ade enter to enter or ade start -f to restart.\n'
                  .format(CFG.name), file=sys.stderr)
            sys.exit(125)
        docker_stop(CFG.name)
        time.sleep(1)

    addargs += CFG.docker_run_args
    run(['docker', 'rm', '-f', CFG.name], check=False, stdout=DEVNULL, stderr=DEVNULL)
    success = await docker_run(name=CFG.name, image=CFG.images[0], home=CFG.home, addargs=addargs,
                               debug=CFG.debug, volume_images=CFG.images[1:],
                               disable_nvidia_docker=CFG.disable_nvidia_docker)

    if not success:
        print('ERROR: Failed to start ade. Re-run with --debug to get more information.',
              file=sys.stderr)
        sys.exit(1)

    if enter:
        docker_exec(CFG.name, broken=CFG.broken_docker_exec)
    else:
        print('\nADE has been started, enter or run commands using: ade enter\n')


@ade.command('stop')
def ade_stop():
    """Stop ade environment."""
    if not ADERC:
        print('ERROR: Needs to run from within project with .aderc file', file=sys.stderr)
        sys.exit(1)

    docker_stop(CFG.name)


@ade.command('save')
@click.argument('directory', type=click.Path(file_okay=False))
def ade_save(directory):
    """Save configuration and images of running ADE into DIRECTORY."""
    if not ADERC:
        print('ERROR: Needs to run from within project with .aderc file', file=sys.stderr)
        sys.exit(1)

    if not is_running(CFG.name):
        print('ERROR: ADE is not running, please start with: ade start', file=sys.stderr)
        sys.exit(1)

    directory = Path(directory).absolute()
    try:
        os.mkdir(directory)
    except FileExistsError:
        if click.confirm(f"Directory must not exists, shall I remove '{directory}'?", abort=True):
            shutil.rmtree(directory)
        os.mkdir(directory)

    shutil.copy(sys.argv[0], directory)
    images = get_label(CFG.name, 'ade_images', dejson=True)
    content = re.sub(r'ADE_IMAGES=".*"',
                     'ADE_IMAGES="{}\n"'.format(''.join([f'\n  {x["fqn"]}' for x in images])),
                     ADERC.read_text(), flags=re.MULTILINE | re.DOTALL)
    (directory / '.aderc').write_text(content)
    (directory / '.ade_images').write_text(json.dumps(images, indent=2, sort_keys=True))
    print('Saving images:')
    for idx, image in enumerate(images):
        path = directory / f'image{idx}.tar.gz'
        print(f'  {image["fqn"]}...', end="", flush=True)
        run(['docker', 'save', image['fqn'], '-o', path])
        print('done.')
    print(f'Images successfully saved to {directory}.')
    (directory / 'README').write_text((
        'Copy this directory to another machine, load images and start ADE.\n\n'
        '  ./ade load .\n'
        '  ./ade start\n'
    ))


@ade.command('load')
@click.argument('directory', type=click.Path(file_okay=False, exists=True))
def ade_load(directory):
    """Load images from DIRECTORY."""
    directory = Path(directory)
    images = json.loads((Path(directory) / '.ade_images').read_text())
    print('Loading images:')
    for idx, image in enumerate(images):
        path = directory / f'image{idx}.tar.gz'
        print(f'  {image["fqn"]}...', end="", flush=True)
        run(['docker', 'load', '-i', path])
    print(f'Images successfully loaded.')


@ade.command('update-cli')
@click.option('--finish-update', help='Used internally by update mechanism')
@asyncio_run
async def ade_update_cli(finish_update):
    """Update ade command-line interface."""
    if not STATICX_PROG_PATH:
        print('ERROR: update-cli is only available for single-binary install.', file=sys.stderr)
        sys.exit(1)

    if finish_update:
        if os.stat(finish_update).st_size < 2**20:
            print(f'Cannot update current ADE version {finish_update}!', file=sys.stderr)
            print(f'New version at {sys.argv[0]}, please update manually.')
            sys.exit(1)
        os.unlink(finish_update)
        shutil.copy(STATICX_PROG_PATH, finish_update)
        print('Update complete')
        os.execl(finish_update, finish_update, '--version')

    project_url = yarl.URL(os.environ.get('ADE_RELEASE_URL', 'https://gitlab.com/ApexAI/ade-cli'))
    gitlab_url = f'{project_url.scheme}://{project_url.host}'
    project_path = '/'.join(project_url.parts[1:])
    async with gitlab.get_api(gitlab_url, '') as api:
        releases = await Release.fetch_all(api, project_path)

    releases = [x for x in releases if x.version > VERSION]
    releases.sort(reverse=True, key=lambda x: x.version)
    if not releases:
        print('ADE cli is up-to-date.')
        return

    print('Newer version available:' + ''.join([f'\n  {x.version!s}' for x in releases]))
    for release in releases:
        if click.confirm(f'Do you want to update to version {release.version!s}'):
            break
    else:
        return

    links = {
        x['name']: x['url']
        for x in release.assets['links']  # pylint: disable=undefined-loop-variable
    }
    tmpdir = Path(tempfile.mkdtemp())
    print('Downloading new version...')
    async with aiohttp.ClientSession() as client:
        async def download(name):
            async with client.get(links[name]) as response:
                assert response.status == 200
                data = await response.read()
                path = (tmpdir / name)
                path.write_bytes(data)
                return path
        name = f'ade+{platform.machine()}'
        _ade, _adesha = await asyncio.gather(download(name), download(f'{name}.sha512sum'))
    print(f'New version downloaded to {tmpdir}')
    # TODO: Handle failure
    run(['sha512sum', '-c', _adesha], stdout=DEVNULL, cwd=tmpdir)
    os.chmod(_ade, 0o755)
    print('Switching to new ADE to finish update:')
    print(f'  {_ade} --finish-update {STATICX_PROG_PATH}')
    os.execl(_ade, _ade, 'update-cli', '--finish-update', STATICX_PROG_PATH)


def cli():
    """Setuptools entrypoint"""
    global ADERC, CFG  # pylint: disable=global-statement
    with launch_pdb_on_exception(PDB):
        ADERC = dotenv.load_dotenv('.aderc')
        CFG = load_config()
        from . import credentials, docker
        credentials.CFG = CFG
        docker.DEBUG = CFG.debug
        prog_name = STATICX_PROG_PATH and Path(STATICX_PROG_PATH).name
        ade(auto_envvar_prefix='ADE', prog_name=prog_name)  # pylint: disable=unexpected-keyword-arg
